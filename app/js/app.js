function initGlobal() {
	$("body").on("contextmenu",function(e){
		return false;
	});
	var pageTag = $(".content").attr("page-tag");
	$(".header-menu ul li a").each(function() {
		if($(this).attr("menu-tag") == pageTag) {
			$(this).addClass('active');
		}
	});
	$(".mobile-menu-button").click(function(){
		$(".header-menu").fadeIn();
		$(this).toggleClass('close');
		$('body').addClass('no-scroll');
		if($(".mobile-menu-button").hasClass('close')) {

		} else {
			$(".header-menu").fadeOut();
			$('body').removeClass('no-scroll');
		}
	});
	if($(window).width() > 900) {
		$(".mobile-menu-button").removeClass('close');
		$(".header-menu").fadeIn();
		$('body').removeClass('no-scroll');
	} else {
		$(".header-menu").hide();
	}
	$(window).resize(function() {
		if($(window).width() > 900) {
			$(".mobile-menu-button").removeClass('close');
			$(".header-menu").fadeIn();
			$('body').removeClass('no-scroll');
		} else {
			$(".header-menu").hide();
		}
	});
}
function index() {
	initGlobal();
}
function category() {
	initGlobal();
	equalheight = function(container){

		var currentTallest = 0,
			currentRowStart = 0,
			rowDivs = new Array(),
			$el,
			topPosition = 0;
		$(container).each(function() {

			$el = $(this);
			$($el).height('auto');
			topPostion = $el.position().top;

			if (currentRowStart != topPostion) {
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
			} else {
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		});
	}

	$(window).load(function() {
		equalheight('.work-info-box');
	});


	$(window).resize(function(){
		equalheight('.work-info-box');
	});
	$('.work-block-slider').slick({
	    infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 3,
			    infinite: true,
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
			    slidesToShow: 2,
			    slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
			    slidesToShow: 1,
			    slidesToScroll: 1
			  }
			}
		]
	});
}
function categorySingle() {
	initGlobal();
}
function schoolAll() {
	initGlobal();
	var emptyCells, i;
	$('.schools-block').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('a').length; i++) {
			emptyCells.push($('<a>', {
			  class: 'is-empty'
			}));
		}
		$(this).append(emptyCells);
	});
}
function theme() {
	initGlobal();
	$(".multi-select").select2({
		placeholder: "Select a subject",
		allowClear: true
	});
}
function schools() {
	initGlobal();
}
function categorySingle() {
	initGlobal();
	$(".multi-select").select2({
		placeholder: "Select a subject",
		allowClear: true
	});
}
function schoolYear() {
	initGlobal();
	$(".multi-select").select2({
		placeholder: "Select a subject",
		allowClear: true
	});
}
function workSingle() {
	initGlobal();
}
function about() {
	initGlobal();
}
function info() {
	initGlobal();
	var len = 80;
	$('p.info-text').each(function() {
		if($(this).text().length>len){
            $(this).attr("title",$(this).text());
            var text=$(this).text().substring(0,len-1)+"...";
            $(this).text(text);
        }
	});
	equalheight = function(container){

		var currentTallest = 0,
			currentRowStart = 0,
			rowDivs = new Array(),
			$el,
			topPosition = 0;
		$(container).each(function() {

			$el = $(this);
			$($el).height('auto');
			topPostion = $el.position().top;

			if (currentRowStart != topPostion) {
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
			} else {
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		});
	}

	$(window).load(function() {
		equalheight('.info-p-box');
	});


	$(window).resize(function(){
		equalheight('.info-p-box');
	});
}
function infoSingle() {
	initGlobal();
}